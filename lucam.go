// lucam package: implements a Go interface around the lucamapi.dll that Lumenera
// uses as a control API for their cameras (including the INFINITY-2).

package lucam

/*
#include <windows.h>

// Go's Syscall-based Windows DLL interface currently has known
// issues with float values, so we use cgo proxy functions for
// calls that send to receive floats

ULONG EnumAvailableFrameRates(void *addr, HANDLE handle, ULONG max, void *frameRates) {
	BOOL (*func)(HANDLE, ULONG, FLOAT *) = addr;
	return func(handle, max, frameRates);
}

BOOL GetFormat(void *addr, HANDLE handle, void *format, void *frameRate) {
	BOOL (*func)(HANDLE, void *, FLOAT *) = addr;
	return func(handle, format, frameRate);
}

BOOL GetProperty(void *addr, HANDLE handle, ULONG property, void *value, void *flags) {
	BOOL (*func)(HANDLE, ULONG, FLOAT *, LONG *) = addr;
	return func(handle, property, (FLOAT *)value, (LONG *)flags);
}

BOOL PropertyRange(void *addr, HANDLE handle, ULONG property, void *min, void *max, void *defaultValue, void *flags) {
	BOOL (*func)(HANDLE, ULONG, FLOAT *, FLOAT *, FLOAT *, LONG *) = addr;
	return func(handle, property, min, max, defaultValue, flags);
}

BOOL SetFormat(void *addr, HANDLE handle, void *format, FLOAT frameRate) {
	BOOL (*func)(HANDLE, void *, FLOAT) = addr;
	return func(handle, format, frameRate);
}

BOOL SetProperty(void *addr, HANDLE handle, ULONG property, FLOAT value, LONG flags) {
	BOOL (*func)(HANDLE, ULONG, FLOAT, LONG) = addr;
	return func(handle, property, value, flags);
}
*/
import "C"

import (
	"fmt"
	"syscall"
	"unsafe"
)

type Version struct {
	Firmware     uint32
	FGPA         uint32
	Api          uint32
	Driver       uint32
	SerialNumber uint32
	reserved     uint32
}

type FrameFormat struct {
	XOffset     uint32
	YOffset     uint32
	Width       uint32
	Height      uint32
	PixelFormat uint32
	SubBinX     uint16
	FlagsX      uint16
	SubBinY     uint16
	FlagsY      uint16
}

type Camera struct {
	handle syscall.Handle
	num    uint32
}

type ControlType uint32

const (
	STOP_STREAMING  ControlType = 0
	START_STREAMING             = 1
	START_DISPLAY               = 2
	PAUSE_STREAM                = 3
	START_RGBSTREAM             = 6
)

var lucamDLL *syscall.DLL

var cameraOpenProc *syscall.Proc
var enumCamerasProc *syscall.Proc
var numCamerasProc *syscall.Proc

var enumAvailableFrameRatesProc uintptr
var getCameraIdProc *syscall.Proc
var getFormatProc uintptr
var getPropertyProc uintptr
var propertyRangeProc uintptr
var queryVersionProc *syscall.Proc
var setPropertyProc uintptr
var setFormatProc uintptr
var streamVideoControlProc *syscall.Proc

func Init() (err error) {
	// If any of the the 'Must' DLL/Proc functions panic
	// return recovery information as an err
	defer func() {
		if r := recover(); r != nil {
			err = fmt.Errorf("%v", r)
		}
	}()

	lucamDLL = syscall.MustLoadDLL("lucamapi.dll")

	cameraOpenProc = lucamDLL.MustFindProc("LucamCameraOpen")
	enumCamerasProc = lucamDLL.MustFindProc("LucamEnumCameras")
	numCamerasProc = lucamDLL.MustFindProc("LucamNumCameras")

	enumAvailableFrameRatesProc = lucamDLL.MustFindProc("LucamEnumAvailableFrameRates").Addr()
	getCameraIdProc = lucamDLL.MustFindProc("LucamGetCameraId")
	getFormatProc = lucamDLL.MustFindProc("LucamGetFormat").Addr()
	getPropertyProc = lucamDLL.MustFindProc("LucamGetProperty").Addr()
	propertyRangeProc = lucamDLL.MustFindProc("LucamPropertyRange").Addr()
	queryVersionProc = lucamDLL.MustFindProc("LucamQueryVersion")
	setPropertyProc = lucamDLL.MustFindProc("LucamSetProperty").Addr()
	setFormatProc = lucamDLL.MustFindProc("LucamSetFormat").Addr()
	streamVideoControlProc = lucamDLL.MustFindProc("LucamStreamVideoControl")

	return
}

func Destroy() {
	if lucamDLL != nil {
		lucamDLL.Release()
	}
}

func CameraOpen(num uint32) (cam Camera, err error) {
	var result uintptr

	if result, _, _ = cameraOpenProc.Call(uintptr(num)); result == 0 {
		err = fmt.Errorf("Unable to open camera number %v", num)
	}

	cam = *&Camera{handle: syscall.Handle(result), num: num}

	return
}

func EnumCameras(max uint32) (versionInfos []Version, err error) {
	if max < 1 {
		err = fmt.Errorf("Invalid parameter passed to EnumCameras (max must be greater than zero)\n")
		return
	}

	var result uintptr
	var versionResults []Version = make([]Version, max)
	var lastErr error

	if result, _, lastErr =
		enumCamerasProc.Call(uintptr(unsafe.Pointer(&versionResults[0])), uintptr(max)); result < 1 {
		err = fmt.Errorf("No cameras enumerated, %v", lastErr)
		return
	}

	count := int32(result)
	versionInfos = make([]Version, count)
	copy(versionInfos, versionResults[0:count])

	return
}

func NumCameras() (num uint32, err error) {
	var result uintptr
	var lastErr error

	if result, _, lastErr = numCamerasProc.Call(); result < 1 {
		err = fmt.Errorf("No cameras found, %v", lastErr)
	}

	num = uint32(result)

	return
}

func (cam *Camera) Number() uint32 {
	return cam.num
}

func (cam *Camera) EnumAvailableFrameRates() (frameRates []float32, err error) {
	max := uint32(C.EnumAvailableFrameRates(unsafe.Pointer(enumAvailableFrameRatesProc),
		C.HANDLE(cam.handle), C.ULONG(0), nil))

	if max > 0 {
		frameRates = make([]float32, max)
	} else {
		err = fmt.Errorf("Camera %v appears to offer no frame rate options", cam.num)
		return
	}

	if count := uint32(C.EnumAvailableFrameRates(
		unsafe.Pointer(enumAvailableFrameRatesProc), C.HANDLE(cam.handle), C.ULONG(max),
		unsafe.Pointer(&frameRates[0]))); count == 0 {
		err = fmt.Errorf("Unable to enum available frame rates for camera %v", cam.num)
	}

	return
}

func (cam *Camera) GetId() (id uint32, err error) {
	if result, _, _ := getCameraIdProc.Call(uintptr(cam.handle), uintptr(unsafe.Pointer(&id))); result == 0 {
		err = fmt.Errorf("Unable to get id for camera number %v", cam.num)
	}

	return
}

func (cam *Camera) GetFormat() (format FrameFormat, frameRate float32, err error) {
	if result := C.GetFormat(
		unsafe.Pointer(getFormatProc), C.HANDLE(cam.handle),
		unsafe.Pointer(&format), unsafe.Pointer(&frameRate)); result == 0 {
		err = fmt.Errorf("Unable to get camera format for camera %v", cam.num)
	}

	return
}

func (cam *Camera) GetProperty(property uint32) (value float32, flags uint32, err error) {
	if result := C.GetProperty(
		unsafe.Pointer(getPropertyProc), C.HANDLE(cam.handle), C.ULONG(property),
		unsafe.Pointer(&value), unsafe.Pointer(&flags)); result == 0 {
		err = fmt.Errorf("Unable to get property %v", property)
	}

	return
}

func (cam *Camera) PropertyRange(property uint32) (min, max, defaultValue float32, flags uint32, err error) {
	if result := C.PropertyRange(unsafe.Pointer(propertyRangeProc),
		C.HANDLE(cam.handle), C.ULONG(property),
		unsafe.Pointer(&min), unsafe.Pointer(&max),
		unsafe.Pointer(&defaultValue), unsafe.Pointer(&flags)); result == 0 {
		err = fmt.Errorf("Unable to determine property range for property: %v", property)
	}

	return
}

func (cam *Camera) QueryVersion() (versionInfo Version, err error) {
	if result, _, _ :=
		queryVersionProc.Call(uintptr(cam.handle), uintptr(unsafe.Pointer(&versionInfo))); result == 0 {
		err = fmt.Errorf("Unable to query camera number %v for version info", cam.num)
	}

	return
}

func (cam *Camera) SetProperty(property uint32, value float32, flags uint32) (err error) {
	if result := C.SetProperty(
		unsafe.Pointer(setPropertyProc), C.HANDLE(cam.handle), C.ULONG(property),
		C.FLOAT(value), C.LONG(flags)); result == 0 {
		err = fmt.Errorf("Unable to set property %v to %v", property, value)
	}

	return
}

func (cam *Camera) SetFormat(format FrameFormat, frameRate float32) (err error) {
	if result := C.SetFormat(
		unsafe.Pointer(setFormatProc), C.HANDLE(cam.handle),
		unsafe.Pointer(&format), C.FLOAT(frameRate)); result == 0 {
		err = fmt.Errorf("Unable to set camera format for camera %v", cam.num)
	}

	return
}

func (cam *Camera) StreamVideoControl(controlType ControlType, hwnd uintptr) (versionInfo Version, err error) {
	if result, _, _ :=
		streamVideoControlProc.Call(uintptr(cam.handle), uintptr(controlType), hwnd); result == 0 {
		err = fmt.Errorf("Unable to initiate StreamVideoControl call for camera number %v", cam.num)
	}

	return
}
